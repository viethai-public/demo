class Food {
    constructor(maMon, tenMon, loaiMon, giaMon, khuyenMai, tinhTrang, hinhAnh, moTa) {
        this.maMon = maMon
        this.tenMon = tenMon
        this.loaiMon = loaiMon
        this.giaMon = giaMon
        this.khuyenMai = khuyenMai
        this.tinhTrang = tinhTrang
        this.hinhAnh = hinhAnh
        this.moTa = moTa
    }
    mapLoaiMon = () => {
        // if (this.loaiMon === 'loai1') return 'Loại Chay'
        // if (this.loaiMon === 'loai2') return 'Loại Mặn'
        return this.loaiMon === 'loai1' ? 'Loại Chay' : 'loại Mặn'
    }
    mapTinhTrang = () => {
        // if (this.tinhTrang === '0') return 'Còn'
        // if (this.tinhTrang === '1') return 'Hết'
        return this.tinhTrang === '0' ? 'Còn' : 'Hết'
    }

    tinhGiaKM = () => {
        return this.giaMon * (1 - (this.khuyenMai * 1) / 100)
    }
}

export default Food

// 1000
// 20
// 1000 - 1000*20%
// 1000(1 - 20%)
// condition ? true: false
