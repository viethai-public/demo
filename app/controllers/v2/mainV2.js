import Food from '../../models/v2/Food.js'

const getElement = (selector) => document.querySelector(selector)

const layThongTinMonAn = () => {
    // debugger
    // ES5
    // const maMon = getElement('#foodID').value
    // const tenMon = getElement('#tenMon').value
    // const loaiMon = getElement('#loai').value
    // const giaMon = getElement('#giaMon').value
    // const khuyenMai = getElement('#khuyenMai').value
    // const tinhTrang = getElement('#tinhTrang').value
    // const hinhAnh = getElement('#hinhMon').value
    // const moTa = getElement('#moTa').value

    // tạo 1 đối tượng từ lớp đối tượng Food
    // const food = new Food(maMon, tenMon, loaiMon, giaMon, khuyenMai, tinhTrang, hinhAnh, moTa)
    // return food
    // console.log('food: ', food)

    // return new Food(maMon, tenMon, loaiMon, giaMon, khuyenMai, tinhTrang, hinhAnh, moTa)

    //ES6
    let food = {}
    const elements = document.querySelectorAll(
        '#foodForm input, #foodForm select, #foodForm textarea'
    )
    elements.forEach((element) => {
        //destructuring
        const { name, value } = element
        food[name] = value
    })
    console.log('food: ', food)

    return new Food(
        food.maMon,
        food.tenMon,
        food.loaiMon,
        food.giaMon,
        food.khuyenMai,
        food.tinhTrang,
        food.hinhAnh,
        food.moTa
    )
}

// Lấy danh sách món ăn từ api
const getFoodList = () => {
    const promise = axios({
        url: 'https://64959f4bb08e17c917926853.mockapi.io/foods',
        method: 'GET',
    })

    promise
        .then((res) => {
            // call API thành công
            console.log('res: ', res)

            // gọi hàm renderTable => hiển thị ds food ngoài ui
            renderTable(res.data)
        })
        .catch((error) => {
            // call API thất bại
            console.log(error)
        })
        .finally(() => {
            // luôn luôn chạy vào sau khi call api bất kể thành công hay thất bại
            console.log('Call API xong')
        })
}

// gọi hàm getFoodList
getFoodList()

// hiển thị danh sách sp ra ngoài ui table
const renderTable = (foodList) => {
    let htmlContent = ''
    foodList.forEach((value, index) => {
        htmlContent += `
        <tr>
        <td>${value.id}</td>
        <td>${value.tenMon}</td>
        <td>${value.maMon}
        <td>${value.giaMon}
        <td>${value.khuyenMai}
        <td>1000</td>
        <td>${value.tinhTrang}</td>
        <td>
            <button 
                class='btn btn-success' 
                data-toggle="modal" 
                data-target="#exampleModal"
                onclick="editFood(${value.id})"
            >
                Edit
            </button>

            <button 
                class='btn btn-danger ml-3' 
                onclick="deleteFood(${value.id})"
            >
                Delete
            </button>
        </td>
        </tr>
        `
    })
    // console.log('htmlContent: ', htmlContent)
    // DOM tới table body hiển thị htmlContent
    document.getElementById('tbodyFood').innerHTML = htmlContent
}

// Thêm món ăn
document.getElementById('btnThemMon').onclick = () => {
    const food = layThongTinMonAn()
    console.log('food: ', food)

    // call API thêm món ăn
    const promise = axios({
        url: 'https://64959f4bb08e17c917926853.mockapi.io/foods',
        method: 'POST',
        data: food,
    })

    promise
        .then((res) => {
            console.log('res: ', res)
            // call API thành công => gọi lại api lấy danh sách sản phẩm
            getFoodList()
        })
        .catch((error) => {
            console.log('error: ', error)
        })
}

// let window ={ }

// xóa sản phẩm
window.deleteFood = (foodId) => {
    console.log('foodId: ', foodId)

    // call API xóa món ăn khỏi data
    const promise = axios({
        url: `https://64959f4bb08e17c917926853.mockapi.io/foods/${foodId}`,
        method: 'DELETE',
    })

    promise
        .then(() => {
            // xóa sản phẩm thành công
            // call lại api get ds sản phẩm
            getFoodList()
        })
        .catch((error) => {
            // xóa thất bại
            console.log('error: ', error)
        })
}

// edit sản phẩm
window.editFood = (foodId) => {
    console.log('foodId: ', foodId)

    // call api lấy thông tin chi tiếp sản phẩm
    const promise = axios({
        url: `https://64959f4bb08e17c917926853.mockapi.io/foods/${foodId}`,
        method: 'GET',
    })

    promise
        .then((res) => {
            console.log(res.data)
            const food = res.data
            getElement('#foodID').value = food.maMon
            getElement('#tenMon').value = food.tenMon
            getElement('#loai').value = food.loaiMon
            getElement('#giaMon').value = food.giaMon
            getElement('#khuyenMai').value = food.khuyenMai
            getElement('#tinhTrang').value = food.tinhTrang
            getElement('#hinhMon').value = food.hinhAnh
            getElement('#moTa').value = food.moTa
        })
        .catch((error) => {
            console.log('error: ', error)
        })
}

// let user = {}
// user.name = 'abc'
