import Food from '../../models/v1/Food.js'

const getElement = (selector) => document.querySelector(selector)

// lấy thông từ user nhập trên form
const layThongTinMonAn = () => {
    // debugger
    // ES5
    // const maMon = getElement('#foodID').value
    // const tenMon = getElement('#tenMon').value
    // const loaiMon = getElement('#loai').value
    // const giaMon = getElement('#giaMon').value
    // const khuyenMai = getElement('#khuyenMai').value
    // const tinhTrang = getElement('#tinhTrang').value
    // const hinhAnh = getElement('#hinhMon').value
    // const moTa = getElement('#moTa').value

    // tạo 1 đối tượng từ lớp đối tượng Food
    // const food = new Food(maMon, tenMon, loaiMon, giaMon, khuyenMai, tinhTrang, hinhAnh, moTa)
    // return food
    // console.log('food: ', food)

    // return new Food(maMon, tenMon, loaiMon, giaMon, khuyenMai, tinhTrang, hinhAnh, moTa)

    //ES6
    let food = {}
    const elements = document.querySelectorAll(
        '#foodForm input, #foodForm select, #foodForm textarea'
    )
    elements.forEach((element) => {
        //destructuring
        const { name, value } = element
        food[name] = value
    })
    console.log('food: ', food)

    return new Food(
        food.maMon,
        food.tenMon,
        food.loaiMon,
        food.giaMon,
        food.khuyenMai,
        food.tinhTrang,
        food.hinhAnh,
        food.moTa
    )
}

getElement('#btnThemMon').onclick = () => {
    const food = layThongTinMonAn()

    getElement('#imgMonAn').src = food.hinhAnh
    getElement('#spMa').innerHTML = food.maMon
    getElement('#spTenMon').innerHTML = food.tenMon
    getElement('#spLoaiMon').innerHTML = food.mapLoaiMon()
    getElement('#spGia').innerHTML = food.giaMon
    getElement('#spKM').innerHTML = food.khuyenMai
    getElement('#spGiaKM').innerHTML = food.tinhGiaKM()
    getElement('#spTT').innerHTML = food.mapTinhTrang()
    getElement('#pMoTa').innerHTML = food.moTa
}

// type: 1,2,3,4
// apdapter
// mapType = 1 => ABC, 2=> ABCDD
// data = {
//     maMon: '3132'
// }

// adapter ('maMon123')

// schema
// {
//     maMon: String
//     giaMon: number
// }

// return {
//     maMon: '123'
// }
